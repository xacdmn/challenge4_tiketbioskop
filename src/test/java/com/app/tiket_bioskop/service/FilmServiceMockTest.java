package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.Films;
import com.app.tiket_bioskop.repository.FilmRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class FilmServiceMockTest {

    @Mock
    private FilmRepository filmRepository;

    private FilmService filmService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.filmService = new FilmServiceImpl(filmRepository);
    }

    @Test
    void addFilm() {
        Films film = new Films();
        Assertions.assertDoesNotThrow(() -> filmService.addFilm(film));
    }

    @Test
    void updateFilm() {
        Films updatedFilm = new Films();
        Assertions.assertDoesNotThrow(() -> filmService.updateFilm(updatedFilm));
    }

    @Test
    void deleteFilm() {
        Assertions.assertDoesNotThrow(() -> filmService.deleteFilm(1));
    }

    @Test
    void deleteScheduleFilm() {
        Assertions.assertDoesNotThrow(() -> filmService.deleteScheduleFilm(1));
    }

    @Test
    void showAllFilms() {
        Assertions.assertEquals(filmRepository.findAll(), filmService.showAllFilms());
    }

    @Test
    void showScheduleFilmNew() {
        Assertions.assertEquals(filmRepository.showFilmSchedule(1), filmService.showFilmSchedule(1));
    }

}
