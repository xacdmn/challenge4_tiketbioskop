package com.app.tiket_bioskop.controller;

import com.app.tiket_bioskop.controller.UserController;
import com.app.tiket_bioskop.dto.AddUserDto;
import com.app.tiket_bioskop.dto.UpdateUserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class UserTest {

    @Autowired
    private UserController userController;

    @Test
    @DisplayName("6. Menambahkan user baru")
    void addUser() {
        AddUserDto addUser = new AddUserDto("OrangNo6", "OrangNo6@gmail.com", "PasswordNo6");
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.CREATED), userController.addUser(addUser));
    }

    @Test
    @DisplayName("7. Mengupdate data User")
    void updateUser() {
        UpdateUserDto updateUser = new UpdateUserDto("OrangNo6", "OrangNo6@gmail.com", "PasswordNo6", 1);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), userController.updateUser(updateUser));
    }

    @Test
    @DisplayName("8. Menghapus user")
    void deleteUser() {
        int userId = 1;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), userController.deleteUser(userId));
    }


}
