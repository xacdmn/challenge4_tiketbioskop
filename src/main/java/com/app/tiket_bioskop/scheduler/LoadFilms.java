package com.app.tiket_bioskop.scheduler;

import com.app.tiket_bioskop.service.FilmService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class LoadFilms extends QuartzJobBean {

    @Autowired
    FilmService filmService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        filmService.showAllFilms();
    }
}