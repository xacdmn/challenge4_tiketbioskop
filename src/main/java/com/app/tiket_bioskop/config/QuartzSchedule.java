package com.app.tiket_bioskop.config;

import com.app.tiket_bioskop.scheduler.LoadFilms;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

@Configuration
public class QuartzSchedule {
    private ApplicationContext applicationContext;

    @Autowired
    public QuartzSchedule(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        SpringBeanJobFactory jobFactory = new SpringBeanJobFactory();
        jobFactory.setApplicationContext(this.applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setJobFactory(springBeanJobFactory());
        JobDetail[] jobDetail = {filmScheduleCheck().getObject()};
        Trigger[] triggers = {filmScheduleCheckTrigger().getObject()};

        schedulerFactoryBean.setJobDetails(jobDetail);
        schedulerFactoryBean.setTriggers(triggers);
        return schedulerFactoryBean;
    }

    @Bean
    public JobDetailFactoryBean filmScheduleCheck() {
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(LoadFilms.class);
        jobDetailFactoryBean.setName("Load Film");
        jobDetailFactoryBean.setDescription("Load all film");
        jobDetailFactoryBean.setDurability(true);
        return jobDetailFactoryBean;
    }

    @Bean
    public CronTriggerFactoryBean filmScheduleCheckTrigger() {
        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(filmScheduleCheck().getObject());
        cronTriggerFactoryBean.setName("Load films every hour");
        cronTriggerFactoryBean.setCronExpression("0 0 * ? * *"); //Every hour
        return cronTriggerFactoryBean;
    }

}
