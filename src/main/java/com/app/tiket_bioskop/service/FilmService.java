package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.Films;
import com.app.tiket_bioskop.entity.Schedules;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    void addFilm(Films films);

    void updateFilm(Films films);

    void deleteFilm(Integer filmId);

    void deleteScheduleFilm(Integer filmId);

    List<Films> showAllFilms();

    List<Schedules> showFilmSchedule(Integer filmId);

    List<Schedules> showScheduleFilm(Integer filmId);
}
