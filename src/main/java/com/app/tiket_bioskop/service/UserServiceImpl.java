package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.user.Users;
import com.app.tiket_bioskop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    public UserServiceImpl (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // CREATE
    @Override
    public void addUser(Users users) {
        String username = users.getUsername();
        String email = users.getEmail();
        String password = users.getPassword();
        userRepository.addUser(username, email, password);
    }

    // UPDATE
    @Override
    public void updateUser(Users users) {
        String username = users.getUsername();
        String email = users.getEmail();
        String password = users.getPassword();
        Integer userId = users.getUserId();
        userRepository.updateUser(username, email, password, userId);
    }

    // DELETE
    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteUser(userId);
    }

    //FOR JASPER
    @Override
    public Users getUserById(Integer userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public List<Users> showAllUsers() {
        return userRepository.findAll();
    }

}
