package com.app.tiket_bioskop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class SeatId implements Serializable {

    private Integer studioName;
    private String seatNumber;
}
