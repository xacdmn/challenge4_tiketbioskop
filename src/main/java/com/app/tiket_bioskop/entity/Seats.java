package com.app.tiket_bioskop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Setter
@Getter
@Entity
public class Seats {

    @EmbeddedId
    private SeatId seatId;
}
