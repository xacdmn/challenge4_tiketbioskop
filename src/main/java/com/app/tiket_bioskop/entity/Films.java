package com.app.tiket_bioskop.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "films")
public class Films {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "film_id")
    private Integer filmId;

    @Column(name = "film_code")
    private String filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "sedang_tayang")
    private Integer sedangTayang;

    public Films() {}

    public Films(Integer filmId) {
        this.filmId = filmId;
    }

}
