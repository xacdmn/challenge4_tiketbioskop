package com.app.tiket_bioskop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "schedules")
@AllArgsConstructor
public class Schedules {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @ManyToOne(targetEntity = Films.class) //cascade = CascadeType.DETACH)
    @JoinColumn(name = "film_id")
    private Films filmId;

    @Column(name = "tanggal_tayang")
    private String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;


    public Schedules() {
    }
}
